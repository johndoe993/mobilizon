## `msgid`s in this file come from POT (.pot) files.
##
## Do not add, change, or remove `msgid`s manually here as
## they're tied to the ones in the corresponding POT file
## (with the same domain).
##
## Use `mix gettext.extract --merge` or `mix gettext.merge`
## to merge POT files into PO files.
msgid ""
msgstr ""
"PO-Revision-Date: 2022-01-24 12:23+0000\n"
"Last-Translator: Al Wisi <alwisi@posteo.de>\n"
"Language-Team: German <https://weblate.framasoft.org/projects/mobilizon/"
"backend-errors/de/>\n"
"Language: de\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.10.1\n"

#, elixir-autogen
#: lib/mobilizon/discussions/discussion.ex:69
msgid "can't be blank"
msgstr "darf nicht leer sein"

msgid "has already been taken"
msgstr "ist bereits vergeben"

msgid "is invalid"
msgstr "ist ungültig"

msgid "must be accepted"
msgstr "muss akzeptiert werden"

msgid "has invalid format"
msgstr "hat ein ungültiges Format"

msgid "has an invalid entry"
msgstr "hat einen ungültigen Eintrag"

msgid "is reserved"
msgstr "ist reserviert"

msgid "does not match confirmation"
msgstr "stimmt nicht mit Bestätigung überein"

msgid "is still associated with this entry"
msgstr "ist noch mit diesem Eintrag verbunden"

msgid "are still associated with this entry"
msgstr "sind noch mit diesem Eintrag verbunden"

msgid "should be %{count} character(s)"
msgid_plural "should be %{count} character(s)"
msgstr[0] "sollte %{count} Zeichen sein"
msgstr[1] "sollten %{count} Zeichen sein"

msgid "should have %{count} item(s)"
msgid_plural "should have %{count} item(s)"
msgstr[0] "sollte ein Stück haben"
msgstr[1] "sollte %{count} Stück haben"

msgid "should be at least %{count} character(s)"
msgid_plural "should be at least %{count} character(s)"
msgstr[0] "sollte mindestens ein Zeichen haben"
msgstr[1] "sollte mindestens %{count} Zeichen haben"

msgid "should have at least %{count} item(s)"
msgid_plural "should have at least %{count} item(s)"
msgstr[0] "sollte mindestens einen Artikel haben"
msgstr[1] "sollte mindestens %{count} Artikel haben"

msgid "should be at most %{count} character(s)"
msgid_plural "should be at most %{count} character(s)"
msgstr[0] "sollte höchstens ein Zeichen haben"
msgstr[1] "sollte höchstens %{count} Zeichen haben"

msgid "should have at most %{count} item(s)"
msgid_plural "should have at most %{count} item(s)"
msgstr[0] "sollte maximal einen Artikel haben"
msgstr[1] "sollte maximal %{count} Artikel haben"

msgid "must be less than %{number}"
msgstr "muss kleiner sein als %{number}"

msgid "must be greater than %{number}"
msgstr "muss kleiner sein als %{number}"

msgid "must be less than or equal to %{number}"
msgstr "muss kleiner oder gleich %{number} sein"

msgid "must be greater than or equal to %{number}"
msgstr "muss größer oder gleich %{number} sein"

msgid "must be equal to %{number}"
msgstr "muss gleich %{number} sein"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/user.ex:107
msgid "Cannot refresh the token"
msgstr "Der Token konnte nicht aktualisiert werden"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/group.ex:272
msgid "Current profile is not a member of this group"
msgstr "Aktuelles Profil ist nicht Mitglied dieser Gruppe"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/group.ex:276
msgid "Current profile is not an administrator of the selected group"
msgstr "Aktuelles Profil ist kein Administrator der ausgewählten Gruppe"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/user.ex:610
msgid "Error while saving user settings"
msgstr "Fehler beim Speichern von Benutzereinstellungen"

#, elixir-autogen, elixir-format
#: lib/graphql/error.ex:101
#: lib/graphql/resolvers/group.ex:269
#: lib/graphql/resolvers/group.ex:301
#: lib/graphql/resolvers/group.ex:338
#: lib/graphql/resolvers/group.ex:369
#: lib/graphql/resolvers/group.ex:418
#: lib/graphql/resolvers/member.ex:81
msgid "Group not found"
msgstr "Gruppe nicht gefunden"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/group.ex:98
#: lib/graphql/resolvers/group.ex:102
msgid "Group with ID %{id} not found"
msgstr "Gruppe mit der ID %{id} nicht gefunden"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/user.ex:85
msgid "Impossible to authenticate, either your email or password are invalid."
msgstr ""
"Die Authentifizierung ist nicht möglich, entweder Ihre E-Mail oder Ihr "
"Passwort sind ungültig."

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/group.ex:335
msgid "Member not found"
msgstr "Mitglied wurde nicht gefunden"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/actor.ex:94
msgid "No profile found for the moderator user"
msgstr "Kein Profil für den Moderator-Benutzer gefunden"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/user.ex:272
msgid "No user to validate with this email was found"
msgstr ""
"Es wurde kein Benutzer gefunden, der mit dieser E-Mail validiert werden kann"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/person.ex:314
#: lib/graphql/resolvers/user.ex:296
msgid "No user with this email was found"
msgstr "Es wurde kein Benutzer mit dieser E-Mail gefunden"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/feed_token.ex:28
#: lib/graphql/resolvers/participant.ex:32
#: lib/graphql/resolvers/participant.ex:210
#: lib/graphql/resolvers/person.ex:236
#: lib/graphql/resolvers/person.ex:353
#: lib/graphql/resolvers/person.ex:389
#: lib/graphql/resolvers/person.ex:396
#: lib/graphql/resolvers/person.ex:425
#: lib/graphql/resolvers/person.ex:440
msgid "Profile is not owned by authenticated user"
msgstr "Profil ist nicht im Besitz des authentifizierten Benutzers"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/user.ex:159
msgid "Registrations are not open"
msgstr "Registrierungen sind nicht geöffnet"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/user.ex:427
msgid "The current password is invalid"
msgstr "Das aktuelle Passwort ist ungültig"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/admin.ex:334
#: lib/graphql/resolvers/user.ex:470
msgid "The new email doesn't seem to be valid"
msgstr "Die neue E-Mail scheint nicht gültig zu sein"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/admin.ex:337
#: lib/graphql/resolvers/user.ex:473
msgid "The new email must be different"
msgstr "Die neue E-Mail muss anders lauten"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/user.ex:430
msgid "The new password must be different"
msgstr "Das neue Passwort muss anders lauten"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/user.ex:477
#: lib/graphql/resolvers/user.ex:539
#: lib/graphql/resolvers/user.ex:542
msgid "The password provided is invalid"
msgstr "Das angegebene Passwort ist ungültig"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/user.ex:434
msgid "The password you have chosen is too short. Please make sure your password contains at least 6 characters."
msgstr ""
"Das von Ihnen gewählte Passwort ist zu kurz. Bitte stellen Sie sicher, dass "
"Ihr Passwort mindestens 6 Zeichen enthält."

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/user.ex:292
msgid "This user can't reset their password"
msgstr "Dieser Benutzer kann sein Passwort nicht zurücksetzen"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/user.ex:81
msgid "This user has been disabled"
msgstr "Dieser Benutzer wurde deaktiviert"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/user.ex:251
#: lib/graphql/resolvers/user.ex:256
msgid "Unable to validate user"
msgstr "Benutzer kann nicht validiert werden"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/user.ex:520
msgid "User already disabled"
msgstr "Benutzer bereits deaktiviert"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/user.ex:585
msgid "User requested is not logged-in"
msgstr "Angeforderter Benutzer ist nicht eingeloggt"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/group.ex:307
msgid "You are already a member of this group"
msgstr "Sie sind bereits Mitglied in dieser Gruppe"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/group.ex:342
msgid "You can't leave this group because you are the only administrator"
msgstr ""
"Sie können diese Gruppe nicht verlassen, da Sie der einzige Administrator "
"sind"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/group.ex:304
msgid "You cannot join this group"
msgstr "Sie können dieser Gruppe nicht beitreten"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/group.ex:132
msgid "You may not list groups unless moderator."
msgstr "Sie dürfen keine Gruppen auflisten, es sei denn, Sie sind Moderator."

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/user.ex:485
msgid "You need to be logged-in to change your email"
msgstr "Sie müssen eingeloggt sein, um Ihre E-Mail zu ändern"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/user.ex:442
msgid "You need to be logged-in to change your password"
msgstr "Sie müssen eingeloggt sein, um Ihr Passwort zu ändern"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/group.ex:281
msgid "You need to be logged-in to delete a group"
msgstr "Sie müssen eingeloggt sein, um eine Gruppe zu löschen"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/user.ex:547
msgid "You need to be logged-in to delete your account"
msgstr "Sie müssen eingeloggt sein, um Ihr Konto zu löschen"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/group.ex:312
msgid "You need to be logged-in to join a group"
msgstr "Sie müssen eingeloggt sein, um einer Gruppe beizutreten"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/group.ex:347
msgid "You need to be logged-in to leave a group"
msgstr "Sie müssen eingeloggt sein, um eine Gruppe zu verlassen"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/group.ex:245
msgid "You need to be logged-in to update a group"
msgstr "Sie müssen eingeloggt sein, um eine Gruppe zu aktualisieren"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/user.ex:112
msgid "You need to have an existing token to get a refresh token"
msgstr ""
"Sie müssen ein bestehendes Token haben, um ein Refresh-Token zu erhalten"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/user.ex:275
#: lib/graphql/resolvers/user.ex:299
msgid "You requested again a confirmation email too soon"
msgstr "Sie haben erneut eine Bestätigungs-E-Mail zu früh angefordert"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/user.ex:162
msgid "Your email is not on the allowlist"
msgstr "Ihre E-Mail ist nicht in der Zulassungsliste enthalten"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/actor.ex:100
msgid "Error while performing background task"
msgstr "Fehler beim Ausführen einer Hintergrundaufgabe"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/actor.ex:32
msgid "No profile found with this ID"
msgstr "Kein Profil mit dieser ID gefunden"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/actor.ex:61
#: lib/graphql/resolvers/actor.ex:97
msgid "No remote profile found with this ID"
msgstr "Kein entferntes Profil mit dieser ID gefunden"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/actor.ex:72
msgid "Only moderators and administrators can suspend a profile"
msgstr "Nur Moderatoren und Administratoren können ein Profil sperren"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/actor.ex:105
msgid "Only moderators and administrators can unsuspend a profile"
msgstr "Nur Moderatoren und Administratoren können ein Profil unsuspendieren"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/actor.ex:29
msgid "Only remote profiles may be refreshed"
msgstr "Nur entfernte Profile können aufgefrischt werden"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/actor.ex:64
msgid "Profile already suspended"
msgstr "Profil bereits gesperrt"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/participant.ex:96
msgid "A valid email is required by your instance"
msgstr "Eine gültige E-Mail wird von Ihrer Instanz benötigt"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/participant.ex:90
#: lib/graphql/resolvers/participant.ex:143
msgid "Anonymous participation is not enabled"
msgstr "Anonyme Teilnahme ist nicht möglich"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/person.ex:210
msgid "Cannot remove the last administrator of a group"
msgstr "Der letzte Administrator einer Gruppe kann nicht entfernt werden"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/person.ex:207
msgid "Cannot remove the last identity of a user"
msgstr "Kann die letzte Identität eines Benutzers nicht entfernen"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/comment.ex:126
msgid "Comment is already deleted"
msgstr "Kommentar ist bereits gelöscht"

#, elixir-autogen, elixir-format
#: lib/graphql/error.ex:103
#: lib/graphql/resolvers/discussion.ex:69
msgid "Discussion not found"
msgstr "Diskussion nicht gefunden"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/report.ex:71
#: lib/graphql/resolvers/report.ex:90
msgid "Error while saving report"
msgstr "Fehler beim Speichern des Reports"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/report.ex:110
msgid "Error while updating report"
msgstr "Fehler beim Aktualisieren des Reports"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/participant.ex:131
msgid "Event id not found"
msgstr "Veranstaltungs-ID nicht gefunden"

#, elixir-autogen, elixir-format
#: lib/graphql/error.ex:100
#: lib/graphql/resolvers/event.ex:360
#: lib/graphql/resolvers/event.ex:412
msgid "Event not found"
msgstr "Veranstaltung nicht gefunden"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/participant.ex:87
#: lib/graphql/resolvers/participant.ex:128
#: lib/graphql/resolvers/participant.ex:155
#: lib/graphql/resolvers/participant.ex:336
msgid "Event with this ID %{id} doesn't exist"
msgstr "Veranstaltung mit dieser ID %{id} existiert nicht"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/participant.ex:103
msgid "Internal Error"
msgstr "Interner Fehler"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/discussion.ex:219
msgid "No discussion with ID %{id}"
msgstr "Keine Diskussion mit ID %{id}"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/todos.ex:80
#: lib/graphql/resolvers/todos.ex:107
#: lib/graphql/resolvers/todos.ex:179
#: lib/graphql/resolvers/todos.ex:208
#: lib/graphql/resolvers/todos.ex:237
msgid "No profile found for user"
msgstr "Kein Profil für Benutzer gefunden"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/feed_token.ex:64
msgid "No such feed token"
msgstr "Kein solches Feed-Token"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/participant.ex:259
msgid "Participant already has role %{role}"
msgstr "Teilnehmer hat bereits Rolle %{role}"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/participant.ex:187
#: lib/graphql/resolvers/participant.ex:220
#: lib/graphql/resolvers/participant.ex:263
msgid "Participant not found"
msgstr "Teilnehmer nicht gefunden"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/person.ex:32
msgid "Person with ID %{id} not found"
msgstr "Person mit ID %{id} nicht gefunden"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/person.ex:56
msgid "Person with username %{username} not found"
msgstr "Person mit Benutzernamen %{username} nicht gefunden"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/post.ex:169
#: lib/graphql/resolvers/post.ex:203
msgid "Post ID is not a valid ID"
msgstr "Post-ID ist keine gültige ID"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/post.ex:172
#: lib/graphql/resolvers/post.ex:206
msgid "Post doesn't exist"
msgstr "Beitrag existiert nicht"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/member.ex:84
msgid "Profile invited doesn't exist"
msgstr "Eingeladenes Profil existiert nicht Eingeladenes Profil existiert nicht"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/member.ex:93
#: lib/graphql/resolvers/member.ex:97
msgid "Profile is already a member of this group"
msgstr ""
"Profil ist bereits Mitglied in dieser Gruppe Profil ist bereits Mitglied in "
"dieser Gruppe"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/post.ex:133
#: lib/graphql/resolvers/post.ex:175
#: lib/graphql/resolvers/post.ex:209
#: lib/graphql/resolvers/resource.ex:90
#: lib/graphql/resolvers/resource.ex:135
#: lib/graphql/resolvers/resource.ex:168
#: lib/graphql/resolvers/resource.ex:202
#: lib/graphql/resolvers/todos.ex:58
#: lib/graphql/resolvers/todos.ex:83
#: lib/graphql/resolvers/todos.ex:110
#: lib/graphql/resolvers/todos.ex:182
#: lib/graphql/resolvers/todos.ex:214
#: lib/graphql/resolvers/todos.ex:246
msgid "Profile is not member of group"
msgstr "Profil ist nicht Mitglied der Gruppe"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/actor.ex:67
#: lib/graphql/resolvers/person.ex:233
msgid "Profile not found"
msgstr "Profil nicht gefunden"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/report.ex:48
msgid "Report not found"
msgstr "Meldung nicht gefunden"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/resource.ex:172
#: lib/graphql/resolvers/resource.ex:199
msgid "Resource doesn't exist"
msgstr "Ressource ist nicht vorhanden"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/participant.ex:124
msgid "The event has already reached its maximum capacity"
msgstr "Die Veranstaltung hat bereits ihre maximale Kapazität erreicht"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/participant.ex:282
msgid "This token is invalid"
msgstr "Dieses Token ist ungültig"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/todos.ex:176
#: lib/graphql/resolvers/todos.ex:243
msgid "Todo doesn't exist"
msgstr "Todo existiert nicht"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/todos.ex:77
#: lib/graphql/resolvers/todos.ex:211
#: lib/graphql/resolvers/todos.ex:240
msgid "Todo list doesn't exist"
msgstr "ToDo-Liste existiert nicht"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/feed_token.ex:73
msgid "Token does not exist"
msgstr "Token existiert nicht"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/feed_token.ex:67
#: lib/graphql/resolvers/feed_token.ex:70
msgid "Token is not a valid UUID"
msgstr "Token ist keine gültige UUID"

#, elixir-autogen, elixir-format
#: lib/graphql/error.ex:98
msgid "User not found"
msgstr "User nicht gefunden"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/person.ex:310
msgid "You already have a profile for this user"
msgstr "Sie haben bereits ein Profil für diesen Benutzer"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/participant.ex:134
msgid "You are already a participant of this event"
msgstr "Sie sind bereits ein Teilnehmer dieser Veranstaltung"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/member.ex:87
msgid "You are not a member of this group"
msgstr "Sie sind nicht Mitglied in dieser Gruppe"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/member.ex:157
#: lib/graphql/resolvers/member.ex:173
#: lib/graphql/resolvers/member.ex:188
msgid "You are not a moderator or admin for this group"
msgstr "Sie sind kein Moderator oder Admin für diese Gruppe"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/comment.ex:59
msgid "You are not allowed to create a comment if not connected"
msgstr "Wenn Sie nicht verbunden sind, dürfen Sie keinen Kommentar erstellen"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/feed_token.ex:41
msgid "You are not allowed to create a feed token if not connected"
msgstr "Sie dürfen kein Feed-Token erstellen, wenn Sie nicht verbunden sind"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/comment.ex:134
msgid "You are not allowed to delete a comment if not connected"
msgstr "Sie dürfen einen Kommentar nicht löschen, wenn Sie nicht verbunden sind"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/feed_token.ex:82
msgid "You are not allowed to delete a feed token if not connected"
msgstr "Sie dürfen ein Feed-Token nicht löschen, wenn keine Verbindung besteht"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/comment.ex:93
msgid "You are not allowed to update a comment if not connected"
msgstr ""
"Sie dürfen einen Kommentar nicht aktualisieren, wenn Sie nicht verbunden sind"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/participant.ex:181
#: lib/graphql/resolvers/participant.ex:214
msgid "You can't leave event because you're the only event creator participant"
msgstr ""
"Sie können die Veranstaltung nicht verlassen, weil Sie der einzige "
"Teilnehmer sind, der die Veranstaltung erstellt"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/member.ex:192
msgid "You can't set yourself to a lower member role for this group because you are the only administrator"
msgstr ""
"Sie können sich nicht auf eine niedrigere Mitgliedsrolle für diese Gruppe "
"einstellen, da Sie der einzige Administrator sind"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/comment.ex:122
msgid "You cannot delete this comment"
msgstr "Sie können diesen Kommentar nicht löschen"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/event.ex:408
msgid "You cannot delete this event"
msgstr "Sie können diese Veranstaltung nicht löschen"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/member.ex:90
msgid "You cannot invite to this group"
msgstr "Sie können nicht in diese Gruppe einladen"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/feed_token.ex:76
msgid "You don't have permission to delete this token"
msgstr "Sie haben nicht die Berechtigung diesen Token zu löschen"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/admin.ex:56
msgid "You need to be logged-in and a moderator to list action logs"
msgstr ""
"Sie müssen eingeloggt und ein Moderator sein, um Aktionsprotokolle "
"aufzulisten"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/report.ex:36
msgid "You need to be logged-in and a moderator to list reports"
msgstr "Sie müssen eingeloggt und ein Moderator sein, um Berichte aufzulisten"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/report.ex:115
msgid "You need to be logged-in and a moderator to update a report"
msgstr ""
"Sie müssen eingeloggt und ein Moderator sein, um einen Bericht zu "
"aktualisieren"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/report.ex:53
msgid "You need to be logged-in and a moderator to view a report"
msgstr "Sie müssen eingeloggt und ein Moderator sein, um einen Bericht zu sehen"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/admin.ex:250
msgid "You need to be logged-in and an administrator to access admin settings"
msgstr ""
"Sie müssen angemeldet und ein Administrator sein, um auf die Admin-"
"Einstellungen zugreifen zu können"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/admin.ex:234
msgid "You need to be logged-in and an administrator to access dashboard statistics"
msgstr ""
"Sie müssen angemeldet und ein Administrator sein, um auf die Dashboard-"
"Statistiken zugreifen zu können"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/admin.ex:276
msgid "You need to be logged-in and an administrator to save admin settings"
msgstr ""
"Sie müssen eingeloggt und ein Administrator sein, um Admin-Einstellungen zu "
"speichern"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/discussion.ex:84
msgid "You need to be logged-in to access discussions"
msgstr "Sie müssen eingeloggt sein, um auf Diskussionen zugreifen zu können"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/resource.ex:96
msgid "You need to be logged-in to access resources"
msgstr "Sie müssen eingeloggt sein, um auf Ressourcen zugreifen zu können"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/event.ex:318
msgid "You need to be logged-in to create events"
msgstr "Sie müssen eingeloggt sein, um Ereignisse zu erstellen"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/post.ex:141
msgid "You need to be logged-in to create posts"
msgstr "Sie müssen eingeloggt sein, um Beiträge zu erstellen"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/report.ex:87
msgid "You need to be logged-in to create reports"
msgstr "Sie müssen eingeloggt sein, um Berichte zu erstellen"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/resource.ex:140
msgid "You need to be logged-in to create resources"
msgstr "Sie müssen eingeloggt sein, um Ressourcen zu erstellen"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/event.ex:417
msgid "You need to be logged-in to delete an event"
msgstr "Sie müssen eingeloggt sein, um ein Ereignis zu löschen"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/post.ex:214
msgid "You need to be logged-in to delete posts"
msgstr "Sie müssen eingeloggt sein, um Beiträge zu löschen"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/resource.ex:207
msgid "You need to be logged-in to delete resources"
msgstr "Sie müssen eingeloggt sein, um Ressourcen zu löschen"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/participant.ex:108
msgid "You need to be logged-in to join an event"
msgstr "Sie müssen eingeloggt sein, um einer Veranstaltung beizutreten"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/participant.ex:225
msgid "You need to be logged-in to leave an event"
msgstr "Sie müssen eingeloggt sein, um eine Veranstaltung zu verlassen"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/event.ex:374
msgid "You need to be logged-in to update an event"
msgstr "Sie müssen eingeloggt sein, um ein Ereignis zu aktualisieren"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/post.ex:180
msgid "You need to be logged-in to update posts"
msgstr "Sie müssen eingeloggt sein, um Beiträge zu aktualisieren"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/resource.ex:177
msgid "You need to be logged-in to update resources"
msgstr "Sie müssen eingeloggt sein, um Ressourcen zu aktualisieren"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/resource.ex:236
msgid "You need to be logged-in to view a resource preview"
msgstr "Sie müssen eingeloggt sein, um eine Ressourcenvorschau zu sehen"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/resource.ex:132
msgid "Parent resource doesn't belong to this group"
msgstr "Die übergeordnete Ressource gehört nicht zu dieser Gruppe"

#, elixir-autogen, elixir-format
#: lib/mobilizon/users/user.ex:114
msgid "The chosen password is too short."
msgstr "Das gewählte Passwort ist zu kurz."

#, elixir-autogen, elixir-format
#: lib/mobilizon/users/user.ex:142
msgid "The registration token is already in use, this looks like an issue on our side."
msgstr ""
"Das Registrierungs-Token ist bereits in Gebrauch, dies sieht nach einem "
"Problem auf unserer Seite aus."

#, elixir-autogen, elixir-format
#: lib/mobilizon/users/user.ex:108
msgid "This email is already used."
msgstr "Diese E-Mail wird bereits verwendet."

#, elixir-autogen, elixir-format
#: lib/graphql/error.ex:99
msgid "Post not found"
msgstr "Beitrag nicht gefunden"

#, elixir-autogen, elixir-format
#: lib/graphql/error.ex:86
msgid "Invalid arguments passed"
msgstr "Ungültige Argumente übergeben"

#, elixir-autogen, elixir-format
#: lib/graphql/error.ex:92
msgid "Invalid credentials"
msgstr "Ungültige Anmeldeinformationen"

#, elixir-autogen, elixir-format
#: lib/graphql/error.ex:90
msgid "Reset your password to login"
msgstr "Passwort zurücksetzen"

#, elixir-autogen, elixir-format
#: lib/graphql/error.ex:97
#: lib/graphql/error.ex:102
msgid "Resource not found"
msgstr "Ressource nicht gefunden"

#, elixir-autogen, elixir-format
#: lib/graphql/error.ex:104
msgid "Something went wrong"
msgstr "Etwas lief falsch"

#, elixir-autogen, elixir-format
#: lib/graphql/error.ex:85
msgid "Unknown Resource"
msgstr "Unbekannte Ressource"

#, elixir-autogen, elixir-format
#: lib/graphql/error.ex:95
msgid "You don't have permission to do this"
msgstr "Sie haben nicht die Berechtigung dies zu tun"

#, elixir-autogen, elixir-format
#: lib/graphql/error.ex:87
msgid "You need to be logged in"
msgstr "Sie müssen eingeloggt sein"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/member.ex:118
msgid "You can't accept this invitation with this profile."
msgstr "Sie können diese Einladung mit diesem Profil nicht annehmen."

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/member.ex:139
msgid "You can't reject this invitation with this profile."
msgstr "Sie können diese Einladung mit diesem Profil nicht ablehnen."

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/media.ex:71
msgid "File doesn't have an allowed MIME type."
msgstr "Die Datei hat keinen zulässigen MIME-Typ."

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/group.ex:240
msgid "Profile is not administrator for the group"
msgstr "Profil ist nicht Administrator für die Gruppe"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/event.ex:363
msgid "You can't edit this event."
msgstr "Sie können dieses Ereignis nicht bearbeiten."

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/event.ex:366
msgid "You can't attribute this event to this profile."
msgstr "Sie können dieses Ereignis nicht diesem Profil zuordnen."

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/member.ex:142
msgid "This invitation doesn't exist."
msgstr "Diese Einladung gibt es nicht."

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/member.ex:217
msgid "This member already has been rejected."
msgstr "Dieses Mitglied ist bereits abgelehnt worden."

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/member.ex:241
msgid "You don't have the right to remove this member."
msgstr "Sie haben nicht das Recht, dieses Mitglied zu entfernen."

#, elixir-autogen, elixir-format
#: lib/mobilizon/actors/actor.ex:351
msgid "This username is already taken."
msgstr "Dieser Benutzername ist bereits vergeben."

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/discussion.ex:81
msgid "You must provide either an ID or a slug to access a discussion"
msgstr ""
"Sie müssen entweder eine ID oder einen Slug angeben, um auf eine Diskussion "
"zuzugreifen"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/event.ex:313
msgid "Organizer profile is not owned by the user"
msgstr "Organizer-Profil ist nicht im Besitz des Benutzers"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/participant.ex:93
msgid "Profile ID provided is not the anonymous profile one"
msgstr "Die angegebene Profil-ID ist nicht die des anonymen Profils"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/group.ex:181
#: lib/graphql/resolvers/group.ex:223
#: lib/graphql/resolvers/person.ex:148
#: lib/graphql/resolvers/person.ex:182
#: lib/graphql/resolvers/person.ex:304
msgid "The provided picture is too heavy"
msgstr "Das Bild ist zu groß"

#, elixir-autogen, elixir-format
#: lib/web/views/utils.ex:34
msgid "Index file not found. You need to recompile the front-end."
msgstr "Indexdatei nicht gefunden. Sie müssen das Front-End neu kompilieren."

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/resource.ex:129
msgid "Error while creating resource"
msgstr "Fehler beim Speichern des Reports"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/user.ex:503
msgid "Invalid activation token"
msgstr "Ungültiges Aktivierungstoken"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/resource.ex:226
msgid "Unable to fetch resource details from this URL."
msgstr "Die Ressourcendetails können von dieser URL nicht abgerufen werden."

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/event.ex:164
#: lib/graphql/resolvers/participant.ex:253
#: lib/graphql/resolvers/participant.ex:328
msgid "Provided profile doesn't have moderator permissions on this event"
msgstr "Dieses Moderatorenprofil hat keine Berechtigung für diese Veranstaltung"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/event.ex:299
msgid "Organizer profile doesn't have permission to create an event on behalf of this group"
msgstr ""
"Das Veranstalterprofil hat nicht die Berechtigung, eine Veranstaltung im "
"Namen dieser Gruppe zu erstellen"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/event.ex:354
msgid "This profile doesn't have permission to update an event on behalf of this group"
msgstr ""
"Dieses Profil hat nicht die Berechtigung, eine Veranstaltung im Namen dieser "
"Gruppe zu aktualisieren"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/user.ex:166
msgid "Your e-mail has been denied registration or uses a disallowed e-mail provider"
msgstr ""
"Ihre E-Mail wurde für die Registrierung abgelehnt oder Sie verwenden einen "
"nicht zugelassenen E-Mail-Anbieter"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/comment.ex:129
msgid "Comment not found"
msgstr "Veranstaltung nicht gefunden"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/discussion.ex:123
msgid "Error while creating a discussion"
msgstr "Fehler beim Speichern des Reports"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/user.ex:624
msgid "Error while updating locale"
msgstr "Fehler beim Aktualisieren des Reports"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/person.ex:307
msgid "Error while uploading pictures"
msgstr "Fehler beim Aktualisieren des Reports"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/participant.ex:190
msgid "Failed to leave the event"
msgstr "Das Verlassen der Veranstaltung fehlgeschlagen"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/group.ex:236
msgid "Failed to update the group"
msgstr "Aktualisierung der Gruppe fehlgeschlagen"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/admin.ex:331
#: lib/graphql/resolvers/user.ex:467
msgid "Failed to update user email"
msgstr "Das Aktualisieren der E-Mail des Benutzers fehlgeschlagen"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/user.ex:499
msgid "Failed to validate user email"
msgstr "Benutzer kann nicht validiert werden"

#, elixir-autogen, elixir-format, fuzzy
#: lib/graphql/resolvers/participant.ex:146
msgid "The anonymous actor ID is invalid"
msgstr "Die anonyme ID ist ungültig"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/resource.ex:165
msgid "Unknown error while updating resource"
msgstr "Unbekannter Fehler beim Aktualisieren der Ressource"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/comment.ex:84
msgid "You are not the comment creator"
msgstr "Sie sind nicht die Ersteller:in des Kommentars"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/user.ex:424
msgid "You cannot change your password."
msgstr "Sie können Ihr Passwort nicht ändern."

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/participant.ex:321
msgid "Format not supported"
msgstr "Format nicht unterstützt"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/participant.ex:305
msgid "A dependency needed to export to %{format} is not installed"
msgstr ""
"Eine für den Export in %{format} erforderliche Abhängigkeit ist nicht "
"installiert"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/participant.ex:313
msgid "An error occured while saving export"
msgstr "Beim Speichern des Exports ist ein Fehler aufgetreten"

#, elixir-autogen, elixir-format
#: lib/web/controllers/export_controller.ex:30
msgid "Export to format %{format} is not enabled on this instance"
msgstr "Export in Format %{format} ist auf dieser Instanz nicht aktivier"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/group.ex:187
msgid "Only admins can create groups"
msgstr "Nur Administratoren können Gruppen erstellen"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/event.ex:306
msgid "Only groups can create events"
msgstr "Nur Gruppen können Veranstaltungen erstellen"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/event.ex:292
msgid "Unknown error while creating event"
msgstr "Unbekannter Fehler beim Erstellen einer Veranstaltung"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/user.ex:480
msgid "User cannot change email"
msgstr "Benutzer:in kann E-Mail nicht ändern"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/group.ex:391
msgid "Follow does not match your account"
msgstr ""

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/group.ex:395
msgid "Follow not found"
msgstr ""

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/user.ex:345
msgid "Profile with username %{username} not found"
msgstr "Person mit Benutzernamen %{username} nicht gefunden"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/user.ex:340
msgid "This profile does not belong to you"
msgstr "Dieses Profil gehört nicht Ihnen"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/group.ex:365
msgid "You are already following this group"
msgstr "Sie folgen dieser Gruppe bereits"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/group.ex:374
msgid "You need to be logged-in to follow a group"
msgstr "Sie müssen eingeloggt sein, um einer Gruppe beizutreten"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/group.ex:423
msgid "You need to be logged-in to unfollow a group"
msgstr "Sie müssen eingeloggt sein, um einer Gruppe beizutreten"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/group.ex:400
msgid "You need to be logged-in to update a group follow"
msgstr "Sie müssen eingeloggt sein, um eine Gruppe zu aktualisieren"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/member.ex:210
msgid "This member does not exist"
msgstr "Diese Mitglied:erin existiert nicht"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/member.ex:234
msgid "You don't have the role needed to remove this member."
msgstr "Sie haben nicht das Recht, dieses Mitglied zu entfernen."

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/member.ex:252
msgid "You must be logged-in to remove a member"
msgstr "Sie müssen angemeldet sein, um eine Mitglied:erin zu entfernen"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/user.ex:156
msgid "Your email seems to be using an invalid format"
msgstr "Ihre E-Mail scheint ein ungültiges Format zu verwenden"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/admin.ex:380
msgid "Can't confirm an already confirmed user"
msgstr "Kann einen bereits bestätigten Benutzer nicht bestätigen"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/admin.ex:384
msgid "Deconfirming users is not supported"
msgstr ""
"Das Zurücknehmen einer Bestätigung von Benutzer:innen wird nicht unterstützt"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/admin.ex:356
msgid "The new role must be different"
msgstr "Die neue Rolle muss anders sein"

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/admin.ex:307
msgid "You need to be logged-in and an administrator to edit an user's details"
msgstr ""
"Sie müssen angemeldet und ein Administrator:in sein, um Administrator:in-"
"Einstellungen zu ändern"

#, elixir-autogen, elixir-format
#: lib/graphql/api/groups.ex:33
msgid "A profile or group with that name already exists"
msgstr ""

#, elixir-autogen, elixir-format
#: lib/graphql/resolvers/admin.ex:506
msgid "Unable to find an instance to follow at this address"
msgstr ""
