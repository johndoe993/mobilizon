## "msgid"s in this file come from POT (.pot) files.
##
## Do not add, change, or remove "msgid"s manually here as
## they're tied to the ones in the corresponding POT file
## (with the same domain).
##
## Use "mix gettext.extract --merge" or "mix gettext.merge"
## to merge POT files into PO files.
msgid ""
msgstr ""
"Language: nb_NO\n"
"Plural-Forms: nplurals=2\n"

#, elixir-autogen, elixir-format
#: lib/web/templates/email/activity/_member_activity_item.html.heex:14
#: lib/web/templates/email/activity/_member_activity_item.text.eex:12
msgid "%{member} accepted the invitation to join the group."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/templates/email/activity/_member_activity_item.html.heex:19
#: lib/web/templates/email/activity/_member_activity_item.text.eex:17
msgid "%{member} rejected the invitation to join the group."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/templates/email/activity/_member_activity_item.html.heex:3
#: lib/web/templates/email/activity/_member_activity_item.text.eex:1
msgid "%{member} requested to join the group."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/templates/email/activity/_member_activity_item.html.heex:8
#: lib/web/templates/email/activity/_member_activity_item.text.eex:6
msgid "%{member} was invited by %{profile}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/templates/email/activity/_member_activity_item.html.heex:30
#: lib/web/templates/email/activity/_member_activity_item.text.eex:27
msgid "%{profile} added the member %{member}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/templates/email/activity/_discussion_activity_item.html.heex:27
#: lib/web/templates/email/activity/_discussion_activity_item.text.eex:19
msgid "%{profile} archived the discussion %{discussion}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/templates/email/activity/_discussion_activity_item.html.heex:3
#: lib/web/templates/email/activity/_discussion_activity_item.text.eex:1
msgid "%{profile} created the discussion %{discussion}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/templates/email/activity/_resource_activity_item.html.heex:4
#: lib/web/templates/email/activity/_resource_activity_item.text.eex:2
msgid "%{profile} created the folder %{resource}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/templates/email/activity/_group_activity_item.html.heex:3
#: lib/web/templates/email/activity/_group_activity_item.text.eex:1
msgid "%{profile} created the group %{group}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/templates/email/activity/_resource_activity_item.html.heex:14
#: lib/web/templates/email/activity/_resource_activity_item.text.eex:8
msgid "%{profile} created the resource %{resource}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/templates/email/activity/_discussion_activity_item.html.heex:35
#: lib/web/templates/email/activity/_discussion_activity_item.text.eex:25
msgid "%{profile} deleted the discussion %{discussion}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/templates/email/activity/_resource_activity_item.html.heex:80
#: lib/web/templates/email/activity/_resource_activity_item.text.eex:40
msgid "%{profile} deleted the folder %{resource}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/templates/email/activity/_resource_activity_item.html.heex:86
#: lib/web/templates/email/activity/_resource_activity_item.text.eex:45
msgid "%{profile} deleted the resource %{resource}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/templates/email/activity/_member_activity_item.html.heex:42
#: lib/web/templates/email/activity/_member_activity_item.text.eex:39
msgid "%{profile} excluded member %{member}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/templates/email/activity/_resource_activity_item.html.heex:58
#: lib/web/templates/email/activity/_resource_activity_item.text.eex:28
msgid "%{profile} moved the folder %{resource}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/templates/email/activity/_resource_activity_item.html.heex:68
#: lib/web/templates/email/activity/_resource_activity_item.text.eex:34
msgid "%{profile} moved the resource %{resource}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/templates/email/activity/_member_activity_item.html.heex:48
#: lib/web/templates/email/activity/_member_activity_item.text.eex:45
msgid "%{profile} quit the group."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/templates/email/activity/_discussion_activity_item.html.heex:19
#: lib/web/templates/email/activity/_discussion_activity_item.text.eex:13
msgid "%{profile} renamed the discussion %{discussion}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/templates/email/activity/_resource_activity_item.html.heex:26
#: lib/web/templates/email/activity/_resource_activity_item.text.eex:14
msgid "%{profile} renamed the folder from %{old_resource_title} to %{resource}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/templates/email/activity/_resource_activity_item.html.heex:41
#: lib/web/templates/email/activity/_resource_activity_item.text.eex:21
msgid "%{profile} renamed the resource from %{old_resource_title} to %{resource}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/templates/email/activity/_discussion_activity_item.html.heex:11
#: lib/web/templates/email/activity/_discussion_activity_item.text.eex:7
msgid "%{profile} replied to the discussion %{discussion}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/templates/email/activity/_group_activity_item.html.heex:13
#: lib/web/templates/email/activity/_group_activity_item.text.eex:7
msgid "%{profile} updated the group %{group}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/templates/email/activity/_member_activity_item.html.heex:36
#: lib/web/templates/email/activity/_member_activity_item.text.eex:33
msgid "%{profile} updated the member %{member}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/event.ex:23
#: lib/web/templates/email/activity/_event_activity_item.html.heex:3
#: lib/web/templates/email/activity/_event_activity_item.text.eex:1
msgid "The event %{event} was created by %{profile}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/event.ex:43
#: lib/web/templates/email/activity/_event_activity_item.html.heex:23
#: lib/web/templates/email/activity/_event_activity_item.text.eex:13
msgid "The event %{event} was deleted by %{profile}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/event.ex:33
#: lib/web/templates/email/activity/_event_activity_item.html.heex:13
#: lib/web/templates/email/activity/_event_activity_item.text.eex:7
msgid "The event %{event} was updated by %{profile}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/templates/email/activity/_post_activity_item.html.heex:3
#: lib/web/templates/email/activity/_post_activity_item.text.eex:1
msgid "The post %{post} was created by %{profile}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/templates/email/activity/_post_activity_item.html.heex:23
#: lib/web/templates/email/activity/_post_activity_item.text.eex:13
msgid "The post %{post} was deleted by %{profile}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/templates/email/activity/_post_activity_item.html.heex:13
#: lib/web/templates/email/activity/_post_activity_item.text.eex:7
msgid "The post %{post} was updated by %{profile}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/templates/email/activity/_member_activity_item.html.heex:24
#: lib/web/templates/email/activity/_member_activity_item.text.eex:22
msgid "%{member} joined the group."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/event.ex:63
#: lib/web/templates/email/activity/_event_activity_item.html.heex:40
#: lib/web/templates/email/activity/_event_activity_item.text.eex:25
msgid "%{profile} posted a comment on the event %{event}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/event.ex:54
#: lib/web/templates/email/activity/_event_activity_item.html.heex:30
#: lib/web/templates/email/activity/_event_activity_item.text.eex:19
msgid "%{profile} replied to a comment on the event %{event}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/templates/email/email_direct_activity.text.eex:27
msgid "Don't want to receive activity notifications? You may change frequency or disable them in your settings."
msgstr ""

#, elixir-format
#: lib/web/templates/email/email_direct_activity.html.heex:219
#: lib/web/templates/email/email_direct_activity.text.eex:23
msgid "View one more activity"
msgid_plural "View %{count} more activities"
msgstr[0] ""
msgstr[1] ""

#, elixir-format
#: lib/web/templates/email/email_direct_activity.html.heex:53
#: lib/web/templates/email/email_direct_activity.html.heex:60
#: lib/web/templates/email/email_direct_activity.text.eex:6
#: lib/web/templates/email/email_direct_activity.text.eex:7
msgid "There has been an activity!"
msgid_plural "There has been some activity!"
msgstr[0] ""
msgstr[1] ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/renderer.ex:46
msgid "Activity on %{instance}"
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/comment.ex:38
#: lib/web/templates/email/activity/_comment_activity_item.html.heex:13
#: lib/web/templates/email/activity/_comment_activity_item.text.eex:7
#: lib/web/templates/email/email_anonymous_activity.html.heex:48
#: lib/web/templates/email/email_anonymous_activity.text.eex:5
msgid "%{profile} has posted an announcement under event %{event}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/comment.ex:24
#: lib/web/templates/email/activity/_comment_activity_item.html.heex:3
#: lib/web/templates/email/activity/_comment_activity_item.text.eex:1
msgid "%{profile} mentionned you in a comment under event %{event}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/templates/email/email_direct_activity.html.heex:249
msgid "Don't want to receive activity notifications? You may change frequency or disable them in %{tag_start}your settings%{tag_end}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/templates/email/email_direct_activity.html.heex:51
#: lib/web/templates/email/email_direct_activity.text.eex:5
msgid "Here's your weekly activity recap"
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/email/activity.ex:121
#: lib/web/email/activity.ex:142
msgid "Activity notification for %{instance}"
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/email/activity.ex:128
msgid "Daily activity recap for %{instance}"
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/templates/email/email_direct_activity.html.heex:49
#: lib/web/templates/email/email_direct_activity.text.eex:4
msgid "Here's your daily activity recap"
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/email/activity.ex:135
msgid "Weekly activity recap for %{instance}"
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/comment.ex:66
#: lib/web/templates/email/activity/_comment_activity_item.html.heex:34
#: lib/web/templates/email/activity/_comment_activity_item.text.eex:19
msgid "%{profile} has posted a new comment under your event %{event}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/comment.ex:53
#: lib/web/templates/email/activity/_comment_activity_item.html.heex:24
#: lib/web/templates/email/activity/_comment_activity_item.text.eex:13
msgid "%{profile} has posted a new reply under your event %{event}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/web/email/activity.ex:46
msgid "Announcement for your event %{event}"
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/group.ex:23
msgid "The group %{group} was updated by %{profile}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/post.ex:47
msgid "The post %{post} from group %{group} was deleted by %{profile}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/post.ex:31
msgid "The post %{post} from group %{group} was published by %{profile}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/post.ex:39
msgid "The post %{post} from group %{group} was updated by %{profile}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/member.ex:39
msgid "%{member} accepted the invitation to join the group %{group}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/member.ex:47
msgid "%{member} joined the group %{group}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/member.ex:43
msgid "%{member} rejected the invitation to join the group %{group}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/member.ex:31
msgid "%{member} requested to join the group %{group}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/member.ex:35
msgid "%{member} was invited by %{profile} to group %{group}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/member.ex:51
msgid "%{profile} added the member %{member} to group %{group}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/member.ex:55
msgid "%{profile} approved the membership request from %{member} for group %{group}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/resource.ex:33
msgid "%{profile} created the folder %{resource} in group %{group}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/resource.ex:69
msgid "%{profile} deleted the folder %{resource} in group %{group}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/resource.ex:71
msgid "%{profile} deleted the resource %{resource} in group %{group}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/member.ex:75
msgid "%{profile} excluded member %{member} from the group %{group}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/resource.ex:61
msgid "%{profile} moved the folder %{resource} in group %{group}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/resource.ex:63
msgid "%{profile} moved the resource %{resource} in group %{group}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/member.ex:79
msgid "%{profile} quit the group %{group}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/member.ex:63
msgid "%{profile} rejected the membership request from %{member} for group %{group}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/resource.ex:45
msgid "%{profile} renamed the folder from %{old_resource_title} to %{resource} in group %{group}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/resource.ex:51
msgid "%{profile} renamed the resource from %{old_resource_title} to %{resource} in group %{group}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/member.ex:71
msgid "%{profile} updated the member %{member} in group %{group}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/resource.ex:35
msgid "%{profile} created the resource %{resource} in group %{group}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/discussion.ex:86
msgid "%{profile} archived the discussion %{discussion} in group %{group}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/discussion.ex:26
msgid "%{profile} created the discussion %{discussion} in group %{group}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/discussion.ex:101
msgid "%{profile} deleted the discussion %{discussion} in group %{group}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/discussion.ex:56
msgid "%{profile} mentionned you in the discussion %{discussion} in group %{group}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/discussion.ex:71
msgid "%{profile} renamed the discussion %{discussion} in group %{group}."
msgstr ""

#, elixir-autogen, elixir-format
#: lib/service/activity/renderer/discussion.ex:41
msgid "%{profile} replied to the discussion %{discussion} in group %{group}."
msgstr ""
